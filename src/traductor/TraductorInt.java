package traductor;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import static java.lang.System.exit;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import static traductor.Conexion.conn;

/**
 *
 * @author QA
 */
public class TraductorInt extends javax.swing.JFrame {

    /**
     * Creates new form TraductorInt
     */
    DefaultTableModel modelo1 = new DefaultTableModel();
    DefaultTableModel modelo2 = new DefaultTableModel();
    DefaultTableModel modelo3 = new DefaultTableModel();
    DefaultTableModel modelo4 = new DefaultTableModel();
    DefaultTableModel modelo5 = new DefaultTableModel();
    DefaultTableModel modelo6 = new DefaultTableModel();
    DefaultTableModel modelo7 = new DefaultTableModel();
    DefaultTableModel modelo8 = new DefaultTableModel();
    DefaultTableModel modelo9 = new DefaultTableModel();

    private TableRowSorter filtro, filtro1, filtro2,filtro3,filtro5,filtroidioma,filtroPalabra,filtroPalabraEspanol,id_manejSystem;
String texto;
    public TraductorInt() throws SQLException {
        initComponents();
        
        Menu_Principal.setVisible(false);
        Traducto.setVisible(false);

        TextPrompt p = new TextPrompt("Ingrese Idioma", jTextField1);
        TextPrompt r = new TextPrompt("Ingrese País", Busq_pais);

        TextPrompt a = new TextPrompt("Filtrar País", filPais);
        TextPrompt b = new TextPrompt("Filtrar Idioma", filIdioma);

        TextPrompt c = new TextPrompt("Palabra Nueva", palabra);
        TextPrompt re_palabra = new TextPrompt("Ingrese Palabra en español", registro_palabra);
         TextPrompt palabra_espa = new TextPrompt("Filtrar Plabra en Español", palabraEspa);
         TextPrompt fil_Idioma = new TextPrompt("Filtrar Idima", filPais2);
         TextPrompt fil_palabra = new TextPrompt("Filtrar Palabra", filIdioma2);
        
//cracion de modelo
        modelo1.addColumn("CODIGO");
        modelo1.addColumn("País");
        pais.setModel(modelo1);

        modelo2.addColumn("CODIGO");
        modelo2.addColumn("País");
        Pais1.setModel(modelo2);

        modelo3.addColumn("CODIGO");
        modelo3.addColumn("Idioma");
        Idioma1.setModel(modelo3);
        
        modelo4.addColumn("CODIGO");
        modelo4.addColumn("PALABRA");
        tb_palabra_Español.setModel(modelo4);
        
        modelo5.addColumn("IDIOMA");
        jTable1.setModel(modelo5);
        
            modelo6.addColumn("CODIGO");
        modelo6.addColumn("IDIOMA");
        Pais3.setModel(modelo6);
        //Tabla Idiomas
        modelo7.addColumn("CODIGO");
        modelo7.addColumn("PALABRA");
        Idioma3.setModel(modelo7);
        
         modelo8.addColumn("CODIGO");
        modelo8.addColumn("PALABRA");
        Pais4.setModel(modelo8);
        
          modelo9.addColumn("CODIGO");
        modelo9.addColumn("PALABRA");
        jTable2.setModel(modelo9);
        
        Mostrar_País();
        Mostrar_Palabra_Español();
        //Mostrar idiomas en dashboard delete
        Mostrar_Idiomas();
        Mostrar_Pal_Esp();
        Mostrar_palabra_con_Mas_Trad();
  Traducto.setVisible(false);
        Registro_Idioma.setVisible(false);
        Reporte1.setVisible(false);
        Registro_Palabra_Españo.setVisible(false);
        Registro_Palabra.setVisible(false);
        Menu_Botones.setVisible(false);
        Menu_Principal.setVisible(false);
        Manipulación_Datos.setVisible(false);
        Manipulación_Datos_Español.setVisible(false);
          Reporte1.setVisible(false);
    }

    //MOSTRAR DATOS EN LA TABLA
    public void Mostrar_País() throws SQLException {

        String[] datos = new String[9];
        Conexion con = null;
        Statement st = con.conn();
        ResultSet rs = st.executeQuery("select* from TB_Paises");

        try {
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(1).toUpperCase();
                    datos[1] = rs.getString(2).toUpperCase();

                    modelo1.addRow(datos);
                    modelo2.addRow(datos);

                }//end while

            } //end if 
        } catch (SQLException ex) {
        }

    }

    //MOSTRAR DATOS EN LA TABLA
    public void Mostrar_Idioma(String pais) throws SQLException {
        for (int i = 0; i < Idioma1.getRowCount(); i++) {
            modelo3.removeRow(i);
            i -= 1;
        }
        String[] datos = new String[9];
        Conexion con = null;
        Statement st = con.conn();
        ResultSet rs = st.executeQuery("select* from TB_IDIOMAS WHERE ID_PAIS_FK=" + pais);

        try {
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(1).toUpperCase();
                    datos[1] = rs.getString(2).toUpperCase();

                    modelo3.addRow(datos);

                }//end while

            } //end if 
        } catch (SQLException ex) {
        }

    }
    //CONSULTAR PALABRAS EN ESPAÓL
        //MOSTRAR DATOS EN LA TABLA
    public void Mostrar_Palabra_Español() throws SQLException {
        for (int i = 0; i < tb_palabra_Español.getRowCount(); i++) {
            modelo4.removeRow(i);
            i -= 1;
        }
        String[] datos = new String[9];
        Conexion con = null;
        Statement st = con.conn();
        ResultSet rs = st.executeQuery("select*from TB_PALABRA_ESPANOL");

        try {
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = Integer.toString(rs.getInt(1));
                    datos[1] = rs.getString(2);

                    modelo4.addRow(datos);

                }//end while

            } //end if 
        } catch (SQLException ex) {
        }

    }

    public void Registro_Idioma() throws SQLException {

        String[] datos = new String[9];
        Conexion con = null;
        Connection conection = Conexion.con();
        String Sql = "call PR_Reg_Idioma(?,?,?)";

        try {
            CallableStatement cs = conection.prepareCall(Sql);
            cs.setInt(1, Integer.parseInt(id.getText()));
            cs.setString(2, jTextField1.getText());
            cs.registerOutParameter(3, java.sql.Types.INTEGER);
            cs.executeQuery();
            System.out.println("Resultado " + cs.getInt(3));
            if (cs.getInt(3) == 1) {
                JOptionPane.showMessageDialog(null, "<html><h2>El idioma ya se<br>Encuentra registrado a este País </h2></html>\n");

            } else {
                JOptionPane.showMessageDialog(null, "<html><h2>Registro Exitoso</h2></html>");
            }
            cs.close();

        } catch (SQLException ex) {

        }

    }
    //Registrar Palabra
        public void Registro_Palabra() throws SQLException {

        String[] datos = new String[9];
        Conexion con = null;
        Connection conection = Conexion.con();
        String Sql = "call PR_Reg_Palabra(?,?)";

        try {
            CallableStatement cs = conection.prepareCall(Sql);
            cs.setString(1, registro_palabra.getText().toUpperCase());
            cs.registerOutParameter(2, java.sql.Types.INTEGER);
            cs.executeQuery();
            System.out.println("Resultado " + cs.getInt(2));
            if (cs.getInt(2) == 1) {
                JOptionPane.showMessageDialog(null, "<html><h2>La palabra ya se<br>Encuentra en la BD </h2></html>\n");
            } else {
                JOptionPane.showMessageDialog(null, "<html><h2>Registro Exitoso</h2></html>");
            }
            cs.close();

        } catch (SQLException ex) {

        }

    }
         //Registrar Palabra
        public int Consulta_ID_PALABRA() throws SQLException {
        int resultado = 0;
        String[] datos = new String[9];
        Conexion con = null;
        Connection conection = Conexion.con();
        String Sql = "call PR_CNS_ID_Palabra(?)";

        try {
            CallableStatement cs = conection.prepareCall(Sql);
            cs.registerOutParameter(1, java.sql.Types.INTEGER);
            cs.executeQuery();
            ID_TRADUCCION.setText(Integer.toString(cs.getInt(1)));
            System.out.println("Resultado " + cs.getInt(1));
            resultado=cs.getInt(1);
            cs.close();

        } catch (SQLException ex) {

        }
        return resultado;

    }
//Registrar Palabra a idioma
        public int Registro_Palabra_Idioma() throws SQLException {
     int resultado=0;
        String[] datos = new String[9];
        Conexion con = null;
        Connection conection = Conexion.con();
        String Sql = "call PR_Reg_palabra_idioma(?,?,?)";

        try {
            CallableStatement cs = conection.prepareCall(Sql);
            cs.setString(1, id_idioma.getText());
            cs.setString(2, ID_TRADUCCION.getText());
            cs.registerOutParameter(3, java.sql.Types.INTEGER);
            cs.executeQuery();
            System.out.println("Resultado " + cs.getInt(3));
            resultado=cs.getInt(3);
            if (cs.getInt(3) == 1) {
                JOptionPane.showMessageDialog(null, "<html><h2>Registro<br>Erroneo</h2></html>\n");

            } else {
                JOptionPane.showMessageDialog(null, "<html><h2>Registro Exitoso</h2></html>");
            }
            cs.close();

        } catch (SQLException ex) {

        }
        return resultado;

    }
        //registrar traduccion 
        //Registrar Palabra a idioma
        public int Registro_Traduccion() throws SQLException {
     int resultado=0;
        String[] datos = new String[9];
        Conexion con = null;
        Connection conection = Conexion.con();
        String Sql = "call PR_Reg_Traduccion(?,?)";

        try {
            CallableStatement cs = conection.prepareCall(Sql);
            cs.setInt(1,Integer.parseInt( id_palabra_Espanol.getText()));
            cs.setString(2, palabra.getText());
            cs.executeQuery();
            cs.close();

        } catch (SQLException ex) {

        }
        return resultado;

    }
    public void Filtroo() {
        int ColumntaTabla = 1;
        filtro.setRowFilter(RowFilter.regexFilter(Busq_pais.getText(), ColumntaTabla));
    }

    public void Filtroo1() {
        int ColumntaTabla = 1;
        filtro1.setRowFilter(RowFilter.regexFilter(filPais.getText(), ColumntaTabla));
    }
    public void filtroIdiomas(){
      int ColumntaTabla = 1;
        filtroidioma.setRowFilter(RowFilter.regexFilter(filPais2.getText(), ColumntaTabla));
    }
     public void filtroPalabraEspañol(){
      int ColumntaTabla = 1;
        filtroPalabraEspanol.setRowFilter(RowFilter.regexFilter(filPais3.getText(), ColumntaTabla));
    }
        public void filtroPalabra(){
      int ColumntaTabla = 1;
        filtroPalabra.setRowFilter(RowFilter.regexFilter(filIdioma2.getText(), ColumntaTabla));
    }
    public void Filtroo2() {
        int ColumntaTabla = 1;
        filtro2.setRowFilter(RowFilter.regexFilter(filIdioma.getText(), ColumntaTabla));
    }
   public void Filtroo3() {
        int ColumntaTabla = 1;
        filtro3.setRowFilter(RowFilter.regexFilter(palabraEspa.getText(), ColumntaTabla));
    }
   //Filtro idioma
    public void filtroIdiomas_Registradas(){
      int ColumntaTabla = 1;
        id_manejSystem.setRowFilter(RowFilter.regexFilter(jTextField4.getText(), ColumntaTabla));
    }
      public void Mostrar_palabra(String idioma) throws SQLException {
      
        String[] datos = new String[9];
        Conexion con = null;
        Statement st = con.conn();
        ResultSet rs = st.executeQuery("select pal.traduccion from  TB_relacion_idioma rid\n" +
"inner join TB_Idiomas pa\n" +
"on rid.id_idioma_fk=pa.id_idioma\n" +
"inner join TB_Traduccion pal\n" +
"on pal.id_tradiccion=rid.id_palabra_fk\n" +
"inner join tb_palabra_espanol esp\n" +
"on esp.id_palabras=pal.id_palabras_fk\n" +
"WHERE pa.nombre_idioma ='"+idioma+"' AND esp.escritura='"+filtro_Idioma.getText()+"'");

        try {
            if (rs != null) {
                while (rs.next()) {

                   jTextField3.setText(rs.getString(1).toUpperCase());

                }//end while

            } //end if 
        } catch (SQLException ex) {
        }

    }
      
    //Mostrar Idiomas
          //MOSTRAR DATOS EN LA TABLA
    public void Mostrar_Idiomas() throws SQLException {
    for (int i = 0; i < Pais3.getRowCount(); i++) {
            modelo6.removeRow(i);
            i -= 1;
        }
        String[] datos = new String[9];
        Conexion con = null;
        Statement st = con.conn();
        ResultSet rs = st.executeQuery("select id_idioma, nombre_idioma from tb_idiomas");

        try {
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(1).toUpperCase();
                    datos[1] = rs.getString(2).toUpperCase();
System.out.println("IDIOMA"+rs.getString(2));
                    modelo6.addRow(datos);

                }//end while

            } //end if 
        } catch (SQLException ex) {
        }

    }
    //Mostrar palabra Español
    
              //MOSTRAR DATOS EN LA TABLA
    public void Mostrar_Pal_Esp() throws SQLException {
    for (int i = 0; i < Pais4.getRowCount(); i++) {
            modelo8.removeRow(i);
            i -= 1;
        }
        String[] datos = new String[9];
        Conexion con = null;
        Statement st = con.conn();
        ResultSet rs = st.executeQuery("select * from tb_palabra_espanol");

        try {
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(1).toUpperCase();
                    datos[1] = rs.getString(2).toUpperCase();
        System.out.println("Palabra Español"+rs.getString(2));
                    modelo8.addRow(datos);

                }//end while

            } //end if 
        } catch (SQLException ex) {
        }

    }
          public void Mostrar_palabra_IDIOMA(String idioma) throws SQLException {
              for (int i = 0; i < Idioma3.getRowCount(); i++) {
            modelo7.removeRow(i);
            i -= 1;
        }
        String[] datos = new String[9];
        Conexion con = null;
        Statement st = con.conn();
        ResultSet rs = st.executeQuery("select pal.id_tradiccion ,pal.traduccion from  TB_relacion_idioma rid\n" +
"inner join TB_Idiomas pa\n" +
"on rid.id_idioma_fk=pa.id_idioma\n" +
"inner join TB_Traduccion pal\n" +
"on pal.id_tradiccion=rid.id_palabra_fk\n" +
"inner join tb_palabra_espanol esp\n" +
"on esp.id_palabras=pal.id_palabras_fk\n" +
"WHERE pa.nombre_idioma ='"+idioma+"'");

        try {
            if (rs != null) {
                while (rs.next()) {

                  datos[0] = rs.getString(1).toUpperCase();
                    datos[1] = rs.getString(2).toUpperCase();
               System.out.println("PALABRA: "+rs.getString(2)+"\n");
                    modelo7.addRow(datos);

                }//end while

            } //end if 
        } catch (SQLException ex) {
        }

    }
         //ELIMINACION DE PALABRA 
  public int Eliminación_Palabra() throws SQLException {
     int resultado=0;
        String[] datos = new String[9];
        Conexion con = null;
        Connection conection = Conexion.con();
        String Sql = "call PR_Elim_ID_Palabra(?,?)";

        try {
            CallableStatement cs = conection.prepareCall(Sql);
            cs.setInt(1, Integer.parseInt(cod_pal.getText()));
            cs.registerOutParameter(2, java.sql.Types.INTEGER);
            cs.executeQuery();
            System.out.println("Resultado " + cs.getInt(2));
            resultado=cs.getInt(2);
            if (cs.getInt(2) == 1) {
                JOptionPane.showMessageDialog(null, "<html><h2>Eliminacion exitosa<br></h2></html>\n");

            } else {
                JOptionPane.showMessageDialog(null, "<html><h2>Error de Eliminación</h2></html>");
            }
            cs.close();

        } catch (SQLException ex) {

        }
        return resultado;

    }
  //Eliminacion IDOMA
  public int Eliminación_Idioma() throws SQLException {
     int resultado=0;
        String[] datos = new String[9];
        Conexion con = null;
        Connection conection = Conexion.con();
        String Sql = "call PR_Elim_ID(?,?)";

        try {
            CallableStatement cs = conection.prepareCall(Sql);
            cs.setInt(1, Integer.parseInt(GETID.getText()));
            cs.registerOutParameter(2, java.sql.Types.INTEGER);
            cs.executeQuery();
            System.out.println("Resultado " + cs.getInt(2));
            resultado=cs.getInt(2);
            if (cs.getInt(2) == 1) {
                JOptionPane.showMessageDialog(null, "<html><h2>Eliminacion exitosa<br></h2></html>\n");

            } else {
                JOptionPane.showMessageDialog(null, "<html><h2>Error de Eliminación</h2></html>");
            }
            cs.close();

        } catch (SQLException ex) {

        }
        return resultado;

    }
  //Modificación palabra 
   public int Modificación_Palabra() throws SQLException {
     int resultado=0;
        String[] datos = new String[9];
        Conexion con = null;
        Connection conection = Conexion.con();
        String Sql = "call PR_Mod_ID_Palabra(?,?,?)";

        try {
            CallableStatement cs = conection.prepareCall(Sql);
            cs.setString(1, palabraEspa2.getText());
            cs.setInt(2, Integer.parseInt(cod_pal.getText()));
            cs.registerOutParameter(3, java.sql.Types.INTEGER);
            cs.executeQuery();
            System.out.println("Resultado " + cs.getInt(3));
            resultado=cs.getInt(3);
            if (cs.getInt(3) == 1) {
                JOptionPane.showMessageDialog(null, "<html><h2>Modificación exitosa<br></h2></html>\n");

            } else {
                JOptionPane.showMessageDialog(null, "<html><h2>Error de Modificación</h2></html>");
            }
            cs.close();

        } catch (SQLException ex) {

        }
        return resultado;

    }
   
   //Modificación palabra 
   public int Modificación_Idioma() throws SQLException {
     int resultado=0;
        String[] datos = new String[9];
        Conexion con = null;
        Connection conection = Conexion.con();
        String Sql = "call PR_Mod_ID(?,?,?)";

        try {
            CallableStatement cs = conection.prepareCall(Sql);
            cs.setString(1, GetModificarIdioma.getText());
            cs.setInt(2, Integer.parseInt(GETID.getText()));
            cs.registerOutParameter(3, java.sql.Types.INTEGER);
            cs.executeQuery();
            System.out.println("Resultado " + cs.getInt(3));
            resultado=cs.getInt(3);
            if (cs.getInt(3) == 1) {
                JOptionPane.showMessageDialog(null, "<html><h2>Modificación exitosa<br></h2></html>\n");

            } else {
                JOptionPane.showMessageDialog(null, "<html><h2>Error de Modificación</h2></html>");
            }
            cs.close();

        } catch (SQLException ex) {

        }
        return resultado;

    }
   //Eliminacion Palabra
  public int Eliminación_PalabraEspañol() throws SQLException {
     int resultado=0;
        String[] datos = new String[9];
        Conexion con = null;
        Connection conection = Conexion.con();
        String Sql = "call PR_Elim_Palabra(?,?)";

        try {
            CallableStatement cs = conection.prepareCall(Sql);
            cs.setInt(1, Integer.parseInt(cod_pal1.getText()));
            cs.registerOutParameter(2, java.sql.Types.INTEGER);
            cs.executeQuery();
            System.out.println("Resultado " + cs.getInt(2));
            resultado=cs.getInt(2);
            if (cs.getInt(2) == 1) {
                JOptionPane.showMessageDialog(null, "<html><h2>Eliminacion exitosa<br></h2></html>\n");

            } else {
                JOptionPane.showMessageDialog(null, "<html><h2>Error de Eliminación</h2></html>");
            }
            cs.close();

        } catch (SQLException ex) {

        }
        return resultado;

    }
  //Modificación palabra 
   public int Modificación_PalabraEspañol() throws SQLException {
     int resultado=0;
        String[] datos = new String[9];
        Conexion con = null;
        Connection conection = Conexion.con();
        String Sql = "call PR_Mod_Palabra(?,?,?)";

        try {
            CallableStatement cs = conection.prepareCall(Sql);
            cs.setString(1, palabraEspa3.getText());
            cs.setInt(2, Integer.parseInt(cod_pal1.getText()));
            cs.registerOutParameter(3, java.sql.Types.INTEGER);
            cs.executeQuery();
            System.out.println("Resultado " + cs.getInt(3));
            resultado=cs.getInt(3);
            if (cs.getInt(3) == 1) {
                JOptionPane.showMessageDialog(null, "<html><h2>Modificación exitosa<br></h2></html>\n");

            } else {
                JOptionPane.showMessageDialog(null, "<html><h2>Error de Modificación</h2></html>");
            }
            cs.close();

        } catch (SQLException ex) {

        }
        return resultado;

    }
   //Mostrar palabra que tiene mas traduccinoes en otros idiomas
   
          public void Mostrar_palabra_con_Mas_Trad() throws SQLException {
      
        String[] datos = new String[9];
        Conexion con = null;
        Statement st = con.conn();
        ResultSet rs = st.executeQuery("select * from (select COUNT( pal.id_palabras_fk),esp.escritura,pal.id_palabras_fk from  TB_relacion_idioma rid\n" +
"inner join TB_Idiomas pa\n" +
"on rid.id_idioma_fk=pa.id_idioma\n" +
"inner join TB_Traduccion pal\n" +
"on pal.id_tradiccion=rid.id_palabra_fk\n" +
"inner join tb_palabra_espanol esp\n" +
"on esp.id_palabras=pal.id_palabras_fk\n" +
"GROUP BY pal.id_palabras_fk,esp.escritura\n" +
"ORDER BY COUNT( pal.id_palabras_fk) desc) where ROWNUM <= 1");

        try {
            if (rs != null) {
                while (rs.next()) {
                    jLabel12.setText(rs.getString(2).toUpperCase());
                System.out.println(rs.getString(2).toUpperCase());

                }//end while

            } //end if 
        } catch (SQLException ex) {
        }

    }
          
          //Idiomas del sistema
          public void Mostrar_IDIOMA_Registrado() throws SQLException {
    for (int i = 0; i < jTable2.getRowCount(); i++) {
            modelo9.removeRow(i);
            i -= 1;
        }
        String[] datos = new String[9];
        Conexion con = null;
        Statement st = con.conn();
        ResultSet rs = st.executeQuery("select id_idioma, nombre_idioma from tb_idiomas");

        try {
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(1).toUpperCase();
                    datos[1] = rs.getString(2).toUpperCase();
                    modelo9.addRow(datos);

                }//end while

            } //end if 
        } catch (SQLException ex) {
        }

    }
   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Elim_Palabra = new javax.swing.JPopupMenu();
        Eliminar_Palabra = new javax.swing.JMenuItem();
        Mod_Palab = new javax.swing.JPopupMenu();
        Modificar_Palabra = new javax.swing.JMenuItem();
        Elim_Idioma = new javax.swing.JPopupMenu();
        Elimi_Idioma = new javax.swing.JMenuItem();
        Mod_Idioma = new javax.swing.JPopupMenu();
        Modifi_Idioma = new javax.swing.JMenuItem();
        Elim_palabra_es = new javax.swing.JPopupMenu();
        Elimi_palb_esp = new javax.swing.JMenuItem();
        Mod_palabra_es = new javax.swing.JPopupMenu();
        Modif_palb_esp = new javax.swing.JMenuItem();
        Reporte1 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        Manipulación_Datos = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        filIdioma2 = new javax.swing.JTextField();
        jScrollPane10 = new javax.swing.JScrollPane();
        Pais3 = new javax.swing.JTable();
        jScrollPane11 = new javax.swing.JScrollPane();
        Idioma3 = new javax.swing.JTable();
        filPais2 = new javax.swing.JTextField();
        palabraEspa2 = new javax.swing.JTextField();
        cod_pal = new javax.swing.JLabel();
        GetModificarIdioma = new javax.swing.JTextField();
        GETID = new javax.swing.JLabel();
        jToggleButton1 = new javax.swing.JToggleButton();
        Manipulación_Datos_Español = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane12 = new javax.swing.JScrollPane();
        Pais4 = new javax.swing.JTable();
        filPais3 = new javax.swing.JTextField();
        id_idioma3 = new javax.swing.JLabel();
        palabraEspa3 = new javax.swing.JTextField();
        cod_pal1 = new javax.swing.JLabel();
        jToggleButton2 = new javax.swing.JToggleButton();
        Menu_Principal = new javax.swing.JPanel();
        Registro_Idioma = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        pais = new javax.swing.JTable();
        Busq_pais = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        id = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();
        Registro_Palabra_Españo = new javax.swing.JPanel();
        registro_palabra = new javax.swing.JTextField();
        jButton8 = new javax.swing.JButton();
        Registro_Palabra = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        palabra = new javax.swing.JTextField();
        filIdioma = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        Pais1 = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        Idioma1 = new javax.swing.JTable();
        filPais = new javax.swing.JTextField();
        jButton7 = new javax.swing.JButton();
        id_palabra = new javax.swing.JLabel();
        id_idioma = new javax.swing.JLabel();
        jButton10 = new javax.swing.JButton();
        jScrollPane7 = new javax.swing.JScrollPane();
        tb_palabra_Español = new javax.swing.JTable();
        palabraEspa = new javax.swing.JTextField();
        id_palabra_Espanol = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        pal_Selec = new javax.swing.JLabel();
        ID_TRADUCCION = new javax.swing.JLabel();
        Menu_Botones = new javax.swing.JPanel();
        Idiomas = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        Traducto = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        filtro_Idioma = new javax.swing.JTextField();
        Menu = new javax.swing.JPanel();
        jButton9 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();

        Eliminar_Palabra.setText("Eliminar Palabra");
        Eliminar_Palabra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Eliminar_PalabraActionPerformed(evt);
            }
        });
        Elim_Palabra.add(Eliminar_Palabra);

        Modificar_Palabra.setText("Modificar Palabra");
        Modificar_Palabra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Modificar_PalabraActionPerformed(evt);
            }
        });
        Mod_Palab.add(Modificar_Palabra);

        Elimi_Idioma.setText("Eliminar Idioma");
        Elimi_Idioma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Elimi_IdiomaActionPerformed(evt);
            }
        });
        Elim_Idioma.add(Elimi_Idioma);

        Modifi_Idioma.setText("Modificar Idioma");
        Modifi_Idioma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Modifi_IdiomaActionPerformed(evt);
            }
        });
        Mod_Idioma.add(Modifi_Idioma);

        Elimi_palb_esp.setText("Eliminar Palabra");
        Elimi_palb_esp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Elimi_palb_espActionPerformed(evt);
            }
        });
        Elim_palabra_es.add(Elimi_palb_esp);

        Modif_palb_esp.setText("Modificar Palabra");
        Modif_palb_esp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Modif_palb_espActionPerformed(evt);
            }
        });
        Mod_palabra_es.add(Modif_palb_esp);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Reporte1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane6.setViewportView(jTable2);

        Reporte1.add(jScrollPane6, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 80, 190, 220));

        jLabel8.setFont(new java.awt.Font("Segoe UI", 3, 14)); // NOI18N
        jLabel8.setText("Palbra que mas traducciónes ");
        Reporte1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, -1, -1));

        jLabel9.setFont(new java.awt.Font("Segoe UI", 3, 14)); // NOI18N
        jLabel9.setText("Idiomas En el Sistema");
        Reporte1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 0, 150, 30));

        jLabel12.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        Reporte1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, 180, 60));

        jLabel13.setFont(new java.awt.Font("Segoe UI", 3, 14)); // NOI18N
        jLabel13.setText("tiene en otros idiomas");
        Reporte1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 150, 30));

        jTextField4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTextField4MousePressed(evt);
            }
        });
        jTextField4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField4KeyReleased(evt);
            }
        });
        Reporte1.add(jTextField4, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 40, 190, 30));

        getContentPane().add(Reporte1, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 0, 420, 360));

        Manipulación_Datos.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Manipulación_Datos.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel10.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel10.setText("Manipulación Todo los Idiomas");
        Manipulación_Datos.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 290, -1));

        filIdioma2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filIdioma2ActionPerformed(evt);
            }
        });
        filIdioma2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                filIdioma2KeyTyped(evt);
            }
        });
        Manipulación_Datos.add(filIdioma2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 370, 30));

        Pais3.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        Pais3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        Pais3.setComponentPopupMenu(Elim_Idioma);
        Pais3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Pais3MousePressed(evt);
            }
        });
        jScrollPane10.setViewportView(Pais3);

        Manipulación_Datos.add(jScrollPane10, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 370, 70));

        Idioma3.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        Idioma3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        Idioma3.setComponentPopupMenu(Elim_Palabra);
        Idioma3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Idioma3MousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                Idioma3MouseReleased(evt);
            }
        });
        jScrollPane11.setViewportView(Idioma3);

        Manipulación_Datos.add(jScrollPane11, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, 370, 70));

        filPais2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filPais2ActionPerformed(evt);
            }
        });
        filPais2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                filPais2KeyTyped(evt);
            }
        });
        Manipulación_Datos.add(filPais2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 370, 30));

        palabraEspa2.setComponentPopupMenu(Mod_Palab);
        palabraEspa2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                palabraEspa2ActionPerformed(evt);
            }
        });
        palabraEspa2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                palabraEspa2KeyTyped(evt);
            }
        });
        Manipulación_Datos.add(palabraEspa2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 250, 340, 30));
        Manipulación_Datos.add(cod_pal, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 30, 30));

        GetModificarIdioma.setComponentPopupMenu(Mod_Idioma);
        Manipulación_Datos.add(GetModificarIdioma, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 250, 340, 30));
        Manipulación_Datos.add(GETID, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 30, 30));

        jToggleButton1.setText("Español");
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });
        Manipulación_Datos.add(jToggleButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 0, -1, -1));

        getContentPane().add(Manipulación_Datos, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 0, 420, 310));

        Manipulación_Datos_Español.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Manipulación_Datos_Español.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel11.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel11.setText("Manipulación Palabras Español");
        Manipulación_Datos_Español.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 270, -1));

        Pais4.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        Pais4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        Pais4.setComponentPopupMenu(Elim_palabra_es);
        Pais4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Pais4MousePressed(evt);
            }
        });
        jScrollPane12.setViewportView(Pais4);

        Manipulación_Datos_Español.add(jScrollPane12, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, 370, 70));

        filPais3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filPais3ActionPerformed(evt);
            }
        });
        filPais3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                filPais3KeyTyped(evt);
            }
        });
        Manipulación_Datos_Español.add(filPais3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 370, 30));
        Manipulación_Datos_Español.add(id_idioma3, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 0, 30, 20));

        palabraEspa3.setComponentPopupMenu(Mod_palabra_es);
        palabraEspa3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                palabraEspa3ActionPerformed(evt);
            }
        });
        palabraEspa3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                palabraEspa3KeyTyped(evt);
            }
        });
        Manipulación_Datos_Español.add(palabraEspa3, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 210, 320, 30));
        Manipulación_Datos_Español.add(cod_pal1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 210, 30, 30));

        jToggleButton2.setText("Todo los Idiomas");
        jToggleButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton2ActionPerformed(evt);
            }
        });
        Manipulación_Datos_Español.add(jToggleButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 0, -1, -1));

        getContentPane().add(Manipulación_Datos_Español, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 0, 420, 310));

        Menu_Principal.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Registro_Idioma.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Registro_Idioma.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Segoe UI", 2, 36)); // NOI18N
        jLabel1.setText("Registro Idioma");
        Registro_Idioma.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(106, 6, 318, -1));

        jTextField1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        Registro_Idioma.add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 70, 186, 37));

        pais.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        pais.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                paisMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                paisMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(pais);

        Registro_Idioma.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 380, 90));

        Busq_pais.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Busq_paisKeyTyped(evt);
            }
        });
        Registro_Idioma.add(Busq_pais, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 170, 40));

        jLabel3.setText("País Seleccionado:");
        Registro_Idioma.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, 110, 20));
        Registro_Idioma.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 220, 160, 20));
        Registro_Idioma.add(id, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 220, 20, 20));

        jButton6.setText("Registrar");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        Registro_Idioma.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 220, -1, -1));

        Menu_Principal.add(Registro_Idioma, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 420, 280));

        Registro_Palabra_Españo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        registro_palabra.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        Registro_Palabra_Españo.add(registro_palabra, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 90, 320, 40));

        jButton8.setText("Registrar");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        Registro_Palabra_Españo.add(jButton8, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 180, 320, -1));

        Menu_Principal.add(Registro_Palabra_Españo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 410, 310));

        Registro_Palabra.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Registro_Palabra.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel2.setText("Registro Palabra");
        Registro_Palabra.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 180, -1));

        palabra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                palabraActionPerformed(evt);
            }
        });
        Registro_Palabra.add(palabra, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 260, 180, 30));

        filIdioma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filIdiomaActionPerformed(evt);
            }
        });
        filIdioma.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                filIdiomaKeyTyped(evt);
            }
        });
        Registro_Palabra.add(filIdioma, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 180, 30));

        Pais1.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        Pais1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        Pais1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Pais1MousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(Pais1);

        Registro_Palabra.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 180, 70));

        Idioma1.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        Idioma1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        Idioma1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Idioma1MousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                Idioma1MouseReleased(evt);
            }
        });
        jScrollPane3.setViewportView(Idioma1);

        Registro_Palabra.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 180, 180, 70));

        filPais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filPaisActionPerformed(evt);
            }
        });
        filPais.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                filPaisKeyTyped(evt);
            }
        });
        Registro_Palabra.add(filPais, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 180, 30));

        jButton7.setText("Registrar");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        Registro_Palabra.add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 260, 190, 30));
        Registro_Palabra.add(id_palabra, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 0, 30, 20));
        Registro_Palabra.add(id_idioma, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 0, 30, 20));

        jButton10.setText("Registrar Nueva Plabra Español");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });
        Registro_Palabra.add(jButton10, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 200, 200, -1));

        tb_palabra_Español.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tb_palabra_Español.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tb_palabra_EspañolMousePressed(evt);
            }
        });
        jScrollPane7.setViewportView(tb_palabra_Español);

        Registro_Palabra.add(jScrollPane7, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 70, 180, 110));

        palabraEspa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                palabraEspaKeyTyped(evt);
            }
        });
        Registro_Palabra.add(palabraEspa, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 30, 180, 30));
        Registro_Palabra.add(id_palabra_Espanol, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 0, 40, 20));

        jLabel7.setText("Palabra Seleccionada");
        Registro_Palabra.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 180, -1, -1));
        Registro_Palabra.add(pal_Selec, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 200, 130, 20));
        Registro_Palabra.add(ID_TRADUCCION, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 0, 30, 20));

        Menu_Principal.add(Registro_Palabra, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 410, 310));

        Menu_Botones.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Idiomas.setText("Idioma");
        Idiomas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IdiomasActionPerformed(evt);
            }
        });
        Menu_Botones.add(Idiomas, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 97, 20));

        jButton4.setText("Palabra");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        Menu_Botones.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 10, 97, 20));

        Menu_Principal.add(Menu_Botones, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 320, 350, 40));

        getContentPane().add(Menu_Principal, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 0, 410, 360));

        Traducto.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTable1MousePressed(evt);
            }
        });
        jScrollPane4.setViewportView(jTable1);

        Traducto.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 190, 170, 110));
        Traducto.add(jTextField2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, 170, -1));

        jTextField3.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        Traducto.add(jTextField3, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 80, 170, 220));

        jLabel6.setFont(new java.awt.Font("Segoe UI", 2, 36)); // NOI18N
        jLabel6.setText("Traducción");
        Traducto.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 290, -1));

        filtro_Idioma.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                filtro_IdiomaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                filtro_IdiomaKeyTyped(evt);
            }
        });
        Traducto.add(filtro_Idioma, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 70, 170, 30));

        getContentPane().add(Traducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 0, 420, 310));

        Menu.setBackground(new java.awt.Color(255, 255, 255));
        Menu.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Menu.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton9.setText("Reporte");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        Menu.add(jButton9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, 160, 20));

        jButton1.setText("Registro");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        Menu.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 131, 171, -1));

        jButton2.setText("Manipulación de registros");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        Menu.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 165, 171, -1));

        jButton3.setText("Traducción");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        Menu.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 205, 171, -1));

        jButton5.setText("Salir");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        Menu.add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 310, 97, 20));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagen/paises.png"))); // NOI18N
        Menu.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 190, 340));

        getContentPane().add(Menu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 190, 360));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
             
                       Traducto.setVisible(false);
        Registro_Idioma.setVisible(false);
        Reporte1.setVisible(false);
        Registro_Palabra_Españo.setVisible(false);
        Registro_Palabra.setVisible(false);
        Menu_Botones.setVisible(false);
         Manipulación_Datos.setVisible(true);
        try {
            Mostrar_Idiomas();
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        Traducto.setVisible(true);
        Manipulación_Datos.setVisible(false);
        Menu_Principal.setVisible(false);
        Menu_Botones.setVisible(false);
        Registro_Idioma.setVisible(false);
        Reporte1.setVisible(false);
        Registro_Palabra_Españo.setVisible(false);
        Registro_Palabra.setVisible(false);
        Manipulación_Datos_Español.setVisible(false);
      
        

    }//GEN-LAST:event_jButton3ActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void IdiomasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IdiomasActionPerformed
        Registro_Idioma.setVisible(true);
        Registro_Palabra.setVisible(false);
        Registro_Palabra_Españo.setVisible(false);
    }//GEN-LAST:event_IdiomasActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        Registro_Idioma.setVisible(false);
        Registro_Palabra.setVisible(true);
        Registro_Palabra_Españo.setVisible(false);
        try {
            Mostrar_Palabra_Español();
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        exit(0);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void filIdiomaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filIdiomaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_filIdiomaActionPerformed

    private void palabraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_palabraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_palabraActionPerformed

    private void Busq_paisKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Busq_paisKeyTyped
        char c = evt.getKeyChar();
        if (Character.isDigit(c)) {

            evt.consume();
        }
        Busq_pais.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (Busq_pais.getText().toUpperCase());
                Busq_pais.setText(cadena);
                Filtroo();
            }

        });
        filtro = new TableRowSorter(pais.getModel());
        pais.setRowSorter(filtro);
    }//GEN-LAST:event_Busq_paisKeyTyped

    private void paisMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_paisMouseClicked


    }//GEN-LAST:event_paisMouseClicked

    private void paisMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_paisMousePressed
        int seleccion = pais.rowAtPoint(evt.getPoint());
        String nombre = String.valueOf(pais.getValueAt(seleccion, 1));
        int id1 = Integer.parseInt(String.valueOf(pais.getValueAt(seleccion, 0)));

        System.out.println(nombre);
        jLabel4.setText(nombre);
        id.setText(Integer.toString(id1));
    }//GEN-LAST:event_paisMousePressed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        try {
            Registro_Idioma();
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       Manipulación_Datos.setVisible(false);
        Traducto.setVisible(false);
        Registro_Idioma.setVisible(false);
        Reporte1.setVisible(false);
        Registro_Palabra_Españo.setVisible(false);
        Registro_Palabra.setVisible(false);
        Manipulación_Datos_Español.setVisible(false);
        Menu_Principal.setVisible(true);
        Menu_Botones.setVisible(true);

    }//GEN-LAST:event_jButton1ActionPerformed

    private void filPaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filPaisActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_filPaisActionPerformed

    private void filPaisKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filPaisKeyTyped
        char c = evt.getKeyChar();
        if (Character.isDigit(c)) {

            evt.consume();
        }
        filPais.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (filPais.getText().toUpperCase());
                filPais.setText(cadena);
                Filtroo1();
            }

        });
        filtro1 = new TableRowSorter(Pais1.getModel());
        Pais1.setRowSorter(filtro1);
    }//GEN-LAST:event_filPaisKeyTyped

    private void filIdiomaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filIdiomaKeyTyped
        char c = evt.getKeyChar();
        if (Character.isDigit(c)) {

            evt.consume();
        }
        filIdioma.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (filIdioma.getText().toUpperCase());
                filIdioma.setText(cadena);
                Filtroo2();
            }

        });
        filtro2 = new TableRowSorter(Idioma1.getModel());
        Idioma1.setRowSorter(filtro2);
    }//GEN-LAST:event_filIdiomaKeyTyped

    private void Pais1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Pais1MousePressed
        int seleccion = Pais1.rowAtPoint(evt.getPoint());
        String nombre = String.valueOf(Pais1.getValueAt(seleccion, 1));
        int id1 = Integer.parseInt(String.valueOf(Pais1.getValueAt(seleccion, 0)));
            id_palabra.setText(Integer.toString(id1));
        try {
            //   System.out.println(nombre);
            //  jLabel4.setText(nombre);
            //   id.setText(Integer.toString(id1));
            Mostrar_Idioma(Integer.toString(id1));
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_Pais1MousePressed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        try {
          
            //Se realiza el registro de palabra a Idioma
            Registro_Traduccion();
            Consulta_ID_PALABRA();
            Registro_Palabra_Idioma();
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton7ActionPerformed

    private void Idioma1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Idioma1MouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_Idioma1MouseReleased

    private void Idioma1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Idioma1MousePressed
      int seleccion = Idioma1.rowAtPoint(evt.getPoint());
        String nombre = String.valueOf(Idioma1.getValueAt(seleccion, 1));
        int id1 = Integer.parseInt(String.valueOf(Idioma1.getValueAt(seleccion, 0)));
            id_idioma.setText(Integer.toString(id1));
    }//GEN-LAST:event_Idioma1MousePressed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        Registro_Palabra_Españo.setVisible(true);
        Registro_Palabra.setVisible(false);
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        try {
            Registro_Palabra();
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton8ActionPerformed

    private void palabraEspaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_palabraEspaKeyTyped
        char c = evt.getKeyChar();
        if (Character.isDigit(c)) {

            evt.consume();
        }
        palabraEspa.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (palabraEspa.getText().toUpperCase());
                palabraEspa.setText(cadena);
                Filtroo3();
            }

        });
        filtro3 = new TableRowSorter(tb_palabra_Español.getModel());
        tb_palabra_Español.setRowSorter(filtro3);
    }//GEN-LAST:event_palabraEspaKeyTyped

    private void tb_palabra_EspañolMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tb_palabra_EspañolMousePressed
         int seleccion = tb_palabra_Español.rowAtPoint(evt.getPoint());
        String nombre = String.valueOf(tb_palabra_Español.getValueAt(seleccion, 1));
        int id1 = Integer.parseInt(String.valueOf(tb_palabra_Español.getValueAt(seleccion, 0)));
            id_palabra_Espanol.setText(Integer.toString(id1));
       pal_Selec.setText(nombre);
    }//GEN-LAST:event_tb_palabra_EspañolMousePressed

    private void filtro_IdiomaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filtro_IdiomaKeyTyped
          char c = evt.getKeyChar();
          texto=String.valueOf(c);  
        
          if(!texto.equals(".")){
         System.out.println("\n"+filtro_Idioma.getText());
          }else{
          
          }
          System.out.println("\n"+texto);
          
        if(c=='.'&&filtro_Idioma.getText().contains(".")){
          evt.consume();
        }
      
  
          
        if ((Character.isDigit(c))) {

            evt.consume();
        }
        

    }//GEN-LAST:event_filtro_IdiomaKeyTyped

    private void filtro_IdiomaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filtro_IdiomaKeyReleased
         if(evt.getKeyCode() == KeyEvent.VK_ENTER){
                  System.out.println("\n"+filtro_Idioma.getText());
       try {
            System.out.println("\n"+"Realización de Busqueda");
            
            for (int i = 0; i < jTable1.getRowCount(); i++) {
                modelo5.removeRow(i);
                i -= 1;
            }
            String[] datos = new String[9];
            Conexion con = null;
            Statement st = con.conn();
            ResultSet rs = st.executeQuery("select pa.nombre_idioma from  TB_relacion_idioma rid\n" +
"inner join TB_Idiomas pa\n" +
"on rid.id_idioma_fk=pa.id_idioma\n" +
"inner join TB_Traduccion pal\n" +
"on pal.id_tradiccion=rid.id_palabra_fk\n" +
"inner join tb_palabra_espanol esp\n" +
"on esp.id_palabras=pal.id_palabras_fk\n" +
"WHERE esp.escritura='"+filtro_Idioma.getText()+"'");
            
            try {
                if (rs != null) {
                    while (rs.next()) {
                        datos[0] = rs.getString(1).toUpperCase();
                      
                        
                        modelo5.addRow(datos);
                        
                    }//end while
                    
                } //end if
            } catch (SQLException ex) {
            }
        } catch (SQLException ex) {
                  Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }   
        }
    }//GEN-LAST:event_filtro_IdiomaKeyReleased

    private void jTable1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MousePressed
       int seleccion = jTable1.rowAtPoint(evt.getPoint());
        String nombre = String.valueOf(jTable1.getValueAt(seleccion, 0));
        try {
            //   System.out.println(nombre);
            //  jLabel4.setText(nombre);
            //   id.setText(Integer.toString(id1));
          Mostrar_palabra(nombre);
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jTable1MousePressed

    private void filIdioma2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filIdioma2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_filIdioma2ActionPerformed

    private void filIdioma2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filIdioma2KeyTyped
         char c = evt.getKeyChar();
        if (Character.isDigit(c)) {

            evt.consume();
        }
        filIdioma2.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (filIdioma2.getText().toUpperCase());
                filIdioma2.setText(cadena);
               filtroPalabra();
            }

        });
        filtroPalabra = new TableRowSorter(Idioma3.getModel());
        Idioma3.setRowSorter(filtroPalabra);
    }//GEN-LAST:event_filIdioma2KeyTyped

    private void Pais3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Pais3MousePressed
        try {
             GetModificarIdioma.setVisible(true);  
            GETID.setVisible(true);  
            cod_pal.setVisible(false);
            palabraEspa2.setVisible(false);
            palabraEspa2.setText("");
            int seleccion = Pais3.rowAtPoint(evt.getPoint());
            String nombre = String.valueOf(Pais3.getValueAt(seleccion, 1));
            String id = String.valueOf(Pais3.getValueAt(seleccion, 0));
            
              System.out.println(nombre);
             GetModificarIdioma.setText(nombre);
             GETID.setText(id);
            //   id.setText(Integer.toString(id1));
            Mostrar_palabra_IDIOMA(nombre);
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }//GEN-LAST:event_Pais3MousePressed

    private void Idioma3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Idioma3MousePressed
            cod_pal.setVisible(true);
            palabraEspa2.setVisible(true );   
            GetModificarIdioma.setVisible(false);  
            GETID.setVisible(false);  
            int seleccion = Idioma3.rowAtPoint(evt.getPoint());
            String nombre = String.valueOf(Idioma3.getValueAt(seleccion, 1));
            String codigo = String.valueOf(Idioma3.getValueAt(seleccion, 0));

              System.out.println(nombre);
              palabraEspa2.setText(nombre);
              cod_pal.setText(codigo);
            //  jLabel4.setText(nombre);
            //   id.setText(Integer.toString(id1));
          
        
    }//GEN-LAST:event_Idioma3MousePressed

    private void Idioma3MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Idioma3MouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_Idioma3MouseReleased

    private void filPais2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filPais2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_filPais2ActionPerformed

    private void filPais2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filPais2KeyTyped
         char c = evt.getKeyChar();
        if (Character.isDigit(c)) {

            evt.consume();
        }
        filPais2.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (filPais2.getText().toUpperCase());
                filPais2.setText(cadena);
                filtroIdiomas();
            }

        });
        filtroidioma = new TableRowSorter(Pais3.getModel());
        Pais3.setRowSorter(filtroidioma);
    }//GEN-LAST:event_filPais2KeyTyped

    private void palabraEspa2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_palabraEspa2KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_palabraEspa2KeyTyped

    private void palabraEspa2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_palabraEspa2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_palabraEspa2ActionPerformed

    private void Eliminar_PalabraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Eliminar_PalabraActionPerformed
        try {
            Eliminación_Palabra();
            Mostrar_palabra_IDIOMA(GetModificarIdioma.getText());
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_Eliminar_PalabraActionPerformed

    private void Modificar_PalabraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Modificar_PalabraActionPerformed
    
        try {
            Modificación_Palabra();
              Mostrar_palabra_IDIOMA(GetModificarIdioma.getText());
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_Modificar_PalabraActionPerformed

    private void Elimi_IdiomaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Elimi_IdiomaActionPerformed
        try {
            Eliminación_Idioma();
            Mostrar_Idiomas();
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_Elimi_IdiomaActionPerformed

    private void Modifi_IdiomaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Modifi_IdiomaActionPerformed
        try {
            Modificación_Idioma();
            Mostrar_Idiomas();
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_Modifi_IdiomaActionPerformed

    private void Pais4MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Pais4MousePressed
  int seleccion = Pais4.rowAtPoint(evt.getPoint());
            String nombre = String.valueOf(Pais4.getValueAt(seleccion, 1));
            String codigo = String.valueOf(Pais4.getValueAt(seleccion, 0));

              System.out.println(nombre);
              palabraEspa3.setText(nombre);
              cod_pal1.setText(codigo);    }//GEN-LAST:event_Pais4MousePressed

    private void filPais3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filPais3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_filPais3ActionPerformed

    private void filPais3KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filPais3KeyTyped
        char c = evt.getKeyChar();
        if (Character.isDigit(c)) {

            evt.consume();
        }
        filPais3.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (filPais3.getText().toUpperCase());
                filPais3.setText(cadena);
                filtroPalabraEspañol();
            }

        });
        filtroPalabraEspanol = new TableRowSorter(Pais4.getModel());
        Pais4.setRowSorter(filtroPalabraEspanol);
    }//GEN-LAST:event_filPais3KeyTyped

    private void palabraEspa3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_palabraEspa3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_palabraEspa3ActionPerformed

    private void palabraEspa3KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_palabraEspa3KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_palabraEspa3KeyTyped

    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed
      Manipulación_Datos.setVisible(false);
      Manipulación_Datos_Español.setVisible(true);
        try {
            Mostrar_Pal_Esp();
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jToggleButton1ActionPerformed

    private void jToggleButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton2ActionPerformed
  Manipulación_Datos.setVisible(true);
Manipulación_Datos_Español.setVisible(false);
        try {
            Mostrar_Idiomas();
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jToggleButton2ActionPerformed

    private void Modif_palb_espActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Modif_palb_espActionPerformed
        try {
            Modificación_PalabraEspañol();
             Mostrar_Pal_Esp();
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_Modif_palb_espActionPerformed

    private void Elimi_palb_espActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Elimi_palb_espActionPerformed
        try {
            Eliminación_PalabraEspañol();
             Mostrar_Pal_Esp();
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_Elimi_palb_espActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        try {
            Mostrar_IDIOMA_Registrado();
        } catch (SQLException ex) {
            Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
        }
        Traducto.setVisible(false);
        Manipulación_Datos.setVisible(false);
        Menu_Principal.setVisible(false);
        Menu_Botones.setVisible(false);
        Registro_Idioma.setVisible(false);
        Reporte1.setVisible(false);
        Registro_Palabra_Españo.setVisible(false);
        Registro_Palabra.setVisible(false);
        Manipulación_Datos_Español.setVisible(false);
 Reporte1.setVisible(true);
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jTextField4MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField4MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4MousePressed

    private void jTextField4KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField4KeyReleased
              char c = evt.getKeyChar();
        if (Character.isDigit(c)) {

            evt.consume();
        }
        jTextField4.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (jTextField4.getText().toUpperCase());
                jTextField4.setText(cadena);
               filtroIdiomas_Registradas();
            }

        });
        id_manejSystem = new TableRowSorter(jTable2.getModel());
        jTable2.setRowSorter(id_manejSystem);
    }//GEN-LAST:event_jTextField4KeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TraductorInt.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TraductorInt.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TraductorInt.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TraductorInt.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new TraductorInt().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(TraductorInt.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Busq_pais;
    private javax.swing.JPopupMenu Elim_Idioma;
    private javax.swing.JPopupMenu Elim_Palabra;
    private javax.swing.JPopupMenu Elim_palabra_es;
    private javax.swing.JMenuItem Elimi_Idioma;
    private javax.swing.JMenuItem Elimi_palb_esp;
    private javax.swing.JMenuItem Eliminar_Palabra;
    private javax.swing.JLabel GETID;
    private javax.swing.JTextField GetModificarIdioma;
    private javax.swing.JLabel ID_TRADUCCION;
    private javax.swing.JTable Idioma1;
    private javax.swing.JTable Idioma3;
    private javax.swing.JButton Idiomas;
    private javax.swing.JPanel Manipulación_Datos;
    private javax.swing.JPanel Manipulación_Datos_Español;
    private javax.swing.JPanel Menu;
    private javax.swing.JPanel Menu_Botones;
    private javax.swing.JPanel Menu_Principal;
    private javax.swing.JPopupMenu Mod_Idioma;
    private javax.swing.JPopupMenu Mod_Palab;
    private javax.swing.JPopupMenu Mod_palabra_es;
    private javax.swing.JMenuItem Modif_palb_esp;
    private javax.swing.JMenuItem Modifi_Idioma;
    private javax.swing.JMenuItem Modificar_Palabra;
    private javax.swing.JTable Pais1;
    private javax.swing.JTable Pais3;
    private javax.swing.JTable Pais4;
    private javax.swing.JPanel Registro_Idioma;
    private javax.swing.JPanel Registro_Palabra;
    private javax.swing.JPanel Registro_Palabra_Españo;
    private javax.swing.JPanel Reporte1;
    private javax.swing.JPanel Traducto;
    private javax.swing.JLabel cod_pal;
    private javax.swing.JLabel cod_pal1;
    private javax.swing.JTextField filIdioma;
    private javax.swing.JTextField filIdioma2;
    private javax.swing.JTextField filPais;
    private javax.swing.JTextField filPais2;
    private javax.swing.JTextField filPais3;
    private javax.swing.JTextField filtro_Idioma;
    private javax.swing.JLabel id;
    private javax.swing.JLabel id_idioma;
    private javax.swing.JLabel id_idioma3;
    private javax.swing.JLabel id_palabra;
    private javax.swing.JLabel id_palabra_Espanol;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JToggleButton jToggleButton2;
    private javax.swing.JTable pais;
    private javax.swing.JLabel pal_Selec;
    private javax.swing.JTextField palabra;
    private javax.swing.JTextField palabraEspa;
    private javax.swing.JTextField palabraEspa2;
    private javax.swing.JTextField palabraEspa3;
    private javax.swing.JTextField registro_palabra;
    private javax.swing.JTable tb_palabra_Español;
    // End of variables declaration//GEN-END:variables
}
