-- Creación de id autoincrementables
  CREATE SEQUENCE  "SYSTEM"."AUITRADUCCION"  MINVALUE 1
  MAXVALUE 9999999999999999999999999999 INCREMENT 
  BY 1 START WITH 101 CACHE 20 ORDER  NOCYCLE ;

  CREATE SEQUENCE  "SYSTEM"."AUIDIDIOMA"  
  MINVALUE 1 MAXVALUE 9999999999999999999999999999 
  INCREMENT BY 1 START WITH 101 CACHE 20 ORDER  NOCYCLE;
  
  CREATE SEQUENCE  "SYSTEM"."AUTPALABRA"  
  MINVALUE 1 MAXVALUE 9999999999999999999999999999 
  INCREMENT BY 1 START WITH 101 CACHE 20 ORDER  NOCYCLE;
  
drop table TB_RELACION_IDIOMA;
DROP TABLE TB_IDIOMAS;
DROP TABLE TB_PAISES;
DROP TABLE TB_PALABRAS;

create table TB_Paises(
id_pais integer primary key,
nombre_pais varchar(50)
);

Create table TB_Palabra_Espanol(
id_palabras integer primary key,
escritura varchar(50)
);


create table TB_Traduccion(
id_tradiccion integer primary key, 
id_palabras_FK integer,
traduccion varchar(50),
FOREIGN KEY (id_palabras_FK) REFERENCES TB_Palabra_Espanol(id_palabras)
);

create table TB_Idiomas(
id_idioma integer primary key,
nombre_idioma varchar(50),
id_pais_fk integer,
FOREIGN KEY (id_pais_fk) REFERENCES TB_Paises(id_pais)
);

create table TB_relacion_idioma(
id_idioma_fk integer,
id_palabra_fk integer,
primary key(id_idioma_fk,id_palabra_fk)
);

--Insersion de Paises
insert into TB_Paises values(1,'Estados Unidos');

insert into TB_Paises values(2,'Rusia');

insert into TB_Paises values(3,'China');

insert into TB_Paises values(4,'Alemania');

--Insersion de Idiomas
insert into TB_Idiomas values(1,'Ingles',1);
insert into TB_Idiomas values(2,'Ruso',2);
insert into TB_Idiomas values(3,'Mandarin',3);
insert into TB_Idiomas values(5,'Aleman',4);

--Insersion de palabras
insert into TB_traduccion values(1,1,'HOME');
insert into TB_traduccion values(2,1,'ДОМ');
insert into TB_traduccion values(3,1,'屋');
insert into TB_traduccion values(4,1,'MAISON');


insert into TB_PALABRA_ESPANOL values(1,'CASA');
insert into TB_PALABRA_ESPANOL values(2,'CABALLO');
insert into TB_PALABRA_ESPANOL values(3,'OBEJA');
insert into TB_PALABRA_ESPANOL values(4,'MANZANA');

--Insersion en relacion idioma
insert into TB_relacion_idioma values(1,1);
insert into TB_relacion_idioma values(2,2);
insert into TB_relacion_idioma values(3,3);
insert into TB_relacion_idioma values(4,4);


--DDL

delete from TB_Idiomas;
delete from TB_Paises;
delete from TB_relacion_idioma;

select* from TB_Paises;
select* from TB_Idiomas;

select*from  TB_relacion_idioma rid
inner join TB_Idiomas pa
on rid.id_idioma_fk=pa.id_idioma
inner join TB_Traduccion pal
on pal.id_tradiccion=rid.id_palabra_fk
inner join tb_palabra_espanol esp
on esp.id_palabras=pal.id_palabras_fk;


--consulta con inner join
select * from TB_Idiomas as idi
inner join TB_Paises pa
on pa.id_pais=idi.id_pais_fk;


--Creación de procedimiento
CREATE OR REPLACE PROCEDURE PR_Reg_Idioma(cod_pais_IN IN Integer,nom_id_IN IN varchar2,resultado out Integer) 
IS
Salida Integer := 0;
BEGIN
SELECT count(*) INTO Salida FROM TB_idiomas WHERE id_pais_fk= cod_pais_IN and nombre_idioma=nom_id_IN;
IF Salida = 0  Then-- obtain my_value with a query or constant, etc.
insert into TB_Idiomas values(AUIDIDIOMA.nextval,nom_id_IN,cod_pais_IN);
resultado:=Salida;
else
resultado:=Salida;
END IF;
END;

declare
texto Integer;
BEGIN
PR_Reg_Idioma(1,'test',texto);
dbms_output.put_line(texto);
END;

--Consultar Ultimo ID Palabra

CREATE OR REPLACE PROCEDURE PR_CNS_ID_Palabra(resultado out Integer) 
IS
Salida Integer := 0;
BEGIN
SELECT id_tradiccion INTO Salida from (select * from TB_Traduccion
order by id_tradiccion desc )
where rownum = 1 ;
resultado:=Salida;
END;


--Consulta id palabra
declare
texto Integer;
BEGIN
PR_CNS_ID_Palabra(texto);
dbms_output.put_line(texto);
END;

--Registro de De Plabras
CREATE OR REPLACE PROCEDURE PR_Reg_Palabra(escritura_IN IN varchar2,resultado out Integer) 
IS
Salida Integer := 0;
BEGIN
SELECT count(*) INTO Salida FROM TB_Palabra_Espanol WHERE escritura=escritura_IN;
IF Salida = 0  Then-- obtain my_value with a query or constant, etc.
insert into TB_Palabra_Espanol values(AUTPALABRA.nextval,escritura_IN);
resultado:=Salida;
else
resultado:=Salida;
END IF;
END;

--Registro de palabras
declare
texto Integer;
BEGIN
PR_Reg_Palabra('TEST',texto);
dbms_output.put_line(texto);
END;

--Registro De traduccion
CREATE OR REPLACE PROCEDURE PR_Reg_Traduccion(id_palabras_IN IN integer,traduccions_IN IN Varchar2) 
IS
BEGIN
insert into TB_Traduccion values(AUITRADUCCION.nextval,id_palabras_IN,traduccions_IN);
END;


--Registro de palabras
declare
texto Integer;
BEGIN
PR_Reg_Traduccion(1,'Caballo');
dbms_output.put_line(texto);
END;

--Eliminación palabra 
CREATE OR REPLACE PROCEDURE PR_Elim_ID_Palabra(id_traducción integer,resultado out Integer) 
IS
Salida Integer := 0;
BEGIN
delete from tb_traduccion WHERE id_tradiccion=id_traducción;
select count(*) into Salida from tb_traduccion WHERE id_tradiccion=id_traducción;
IF Salida = 0  Then-- obtain my_value with a query or constant, etc.
resultado:=1;--Delete exitoso
commit;
else
resultado:=0;
END IF;
END;
--Modificacion palabra 
CREATE OR REPLACE PROCEDURE PR_Mod_ID_Palabra(traduccion_nue varchar2,traduccion_id integer,resultado out Integer) 
IS
Salida Integer := 0;
BEGIN
UPDATE  tb_traduccion set traduccion=traduccion_nue where id_tradiccion=traduccion_id;
select count(*) into Salida from tb_traduccion WHERE traduccion=traduccion_nue;
IF Salida = 1  Then-- obtain my_value with a query or constant, etc.
resultado:=1;--Modificacion exitoso
commit;
else
resultado:=0;
END IF;
END;
//Eliminación idioma
CREATE OR REPLACE PROCEDURE PR_Elim_ID(idioma_id integer,resultado out Integer) 
IS
Salida Integer := 0;
BEGIN
delete from tb_relacion_idioma WHERE id_idioma_fk=idioma_id;
commit;
delete from tb_idiomas WHERE id_idioma=idioma_id;
select count(*) into Salida from tb_idiomas WHERE id_idioma=idioma_id;
IF Salida = 0  Then-- obtain my_value with a query or constant, etc.
resultado:=1;--Delete exitoso
commit;
else
resultado:=0;
END IF;
END;

//modificar IDIOMA
--Modificacion Idioma 
CREATE OR REPLACE PROCEDURE PR_Mod_ID(nombre_nue varchar2,idioma_id integer,resultado out Integer) 
IS
Salida Integer := 0;
BEGIN
UPDATE  tb_idiomas set nombre_idioma=nombre_nue where id_idioma=idioma_id;
select count(*) into Salida from tb_idiomas WHERE nombre_idioma=nombre_nue;
IF Salida = 1  Then-- obtain my_value with a query or constant, etc.
resultado:=1;--Modificacion exitoso
commit;
else
resultado:=0;
END IF;
END;

--Eliminar palabra español
CREATE OR REPLACE PROCEDURE PR_Elim_Palabra(Palabra_id integer,resultado out Integer) 
IS
Salida Integer := 0;
BEGIN
delete from tb_relacion_idioma WHERE id_palabra_fk=Palabra_id;
commit;
delete from tb_traduccion WHERE id_palabras_fk=Palabra_id;
commit;
delete from tb_palabra_espanol WHERE id_palabras=Palabra_id;
select count(*) into Salida from tb_palabra_espanol WHERE id_palabras=Palabra_id;
IF Salida = 0  Then-- obtain my_value with a query or constant, etc.
resultado:=1;--Delete exitoso
commit;
else
resultado:=0;
END IF;
END;
--Modificacion Palabra Español
CREATE OR REPLACE PROCEDURE PR_Mod_Palabra(nombre_nue varchar2,palabra_id integer,resultado out Integer) 
IS
Salida Integer := 0;
BEGIN
UPDATE  tb_palabra_espanol set escritura=nombre_nue where id_palabras=palabra_id;
select count(*) into Salida from tb_palabra_espanol WHERE escritura=nombre_nue;
IF Salida = 1  Then-- obtain my_value with a query or constant, etc.
resultado:=1;--Modificacion exitoso
commit;
else
resultado:=0;
END IF;
END;
---INERJOIN JAVA

select pa.nombre_idioma from  TB_relacion_idioma rid
inner join TB_Idiomas pa
on rid.id_idioma_fk=pa.id_idioma
inner join TB_Traduccion pal
on pal.id_tradiccion=rid.id_palabra_fk
inner join tb_palabra_espanol esp
on esp.id_palabras=pal.id_palabras_fk
WHERE esp.escritura='CASA';

select pal.traduccion from  TB_relacion_idioma rid
inner join TB_Idiomas pa
on rid.id_idioma_fk=pa.id_idioma
inner join TB_Traduccion pal
on pal.id_tradiccion=rid.id_palabra_fk
inner join tb_palabra_espanol esp
on esp.id_palabras=pal.id_palabras_fk
WHERE pa.nombre_idioma ='RUSO' AND esp.escritura='CASA';

select pal.traduccion from  TB_relacion_idioma rid
inner join TB_Idiomas pa
on rid.id_idioma_fk=pa.id_idioma
inner join TB_Traduccion pal
on pal.id_tradiccion=rid.id_palabra_fk
inner join tb_palabra_espanol esp
on esp.id_palabras=pal.id_palabras_fk
WHERE pa.nombre_idioma ='INGLES';
